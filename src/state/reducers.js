import { combineReducers } from 'redux';
import { notificationReducer } from '../common/notifications';
import { assistanceReducer } from '../areas/assistance';

const reducers = {
  notification: notificationReducer,
  assistance: assistanceReducer
};

export default combineReducers(reducers);

import GetAssistance from './GetAssistance.component';
import React from 'react';
import { shallow, mount } from 'enzyme';

describe('GetAssistance component', () => {
  it('should render GetAssistance component correctly with no props', () => {
    const component = shallow(
      <GetAssistance onSubmit={jest.fn()} onDelete={jest.fn()} />
    );
    expect(component).toMatchSnapshot();
  });

  it('should call onSubmit callback when assistance is required (button clicked)', () => {
    const onSubmitFn = jest.fn();

    const component = mount(
      <GetAssistance onSubmit={onSubmitFn} onDelete={jest.fn()} />
    );
    component.find('button#requestBtn').simulate('click');
    expect(onSubmitFn).toHaveBeenCalled();
  });

  it('should call onDelete callback when assistance is required (button clicked)', () => {
    const onDeleteFn = jest.fn();
    const component = mount(
      <GetAssistance onSubmit={jest.fn()} onDelete={onDeleteFn} />
    );
    component.find('button#deleteBtn').simulate('click');
    expect(onDeleteFn).toHaveBeenCalled();
  });
});

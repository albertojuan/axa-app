import assistanceService from './assistance.service';
import actionTypes from './assistance.actionTypes';

function requestAssistance(position) {
  return dispatch => {
    assistanceService
      .pushAssistanceRequest(position)
      .then(() =>
        dispatch({ type: actionTypes.ASSISTANCE_REQUEST_OK, position })
      )
      .catch(err => {
        console.error(err);
        dispatch({ type: actionTypes.ASSISTANCE_REQUEST_KO });
      });
  };
}

function deleteAssistanceRequests() {
  return dispatch => {
    assistanceService
      .removeAssistanceRequests()
      .then(() => dispatch({ type: actionTypes.ASSISTANCE_DELETE_ALL }))
      .catch(err => {
        console.error(err);
        dispatch({ type: actionTypes.ASSISTANCE_REQUEST_KO });
      });
  };
}

export default { requestAssistance, deleteAssistanceRequests };

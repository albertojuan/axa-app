import React, { Fragment } from 'react';
import PropTypes, { number } from 'prop-types';
import { Button, Text } from '../../common/ui';
import styled from 'styled-components';

const MainPanel = styled.div`
  height: 70vh;
  background-color: white;
  padding-top: 15px;
`;

const GetAssistanceComponent = ({
  onSubmit,
  onDelete,
  lastPosition,
  lastUpdate,
  error
}) => {
  return (
    <MainPanel className="showable">
      <Button id="requestBtn" onClick={() => onSubmit()}>
        I need assistance
      </Button>
      <Button id="deleteBtn" onClick={() => onDelete()}>
        Remove all requests
      </Button>

      {lastPosition && (
        <Fragment>
          <Text>Request received</Text>
          <Text important>
            Your position: ({lastPosition.latitude}, {lastPosition.longitude})
          </Text>
          <Text>{lastUpdate.toLocaleString('es-ES')}</Text>
        </Fragment>
      )}
      {error && (
        <Text important error>
          ERROR SENDING POSITION
        </Text>
      )}
    </MainPanel>
  );
};

GetAssistanceComponent.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  lastPosition: PropTypes.shape({
    latitude: number,
    longitude: number
  }),
  lastUpdate: PropTypes.instanceOf(Date),
  error: PropTypes.string
};

export default GetAssistanceComponent;

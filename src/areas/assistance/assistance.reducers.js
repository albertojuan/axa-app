import actionTypes from './assistance.actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case actionTypes.ASSISTANCE_REQUEST_OK:
      return {
        ...state,
        lastPosition: action.position,
        lastUpdate: new Date(),
        error: false
      };

    case actionTypes.ASSISTANCE_DELETE_ALL:
      return {
        ...state,
        lastPosition: null,
        lastUpdate: new Date(),
        error: false
      };

    case actionTypes.ASSISTANCE_REQUEST_KO:
      return {
        ...state,
        lastPosition: null,
        lastUpdate: new Date(),
        error: true
      };

    default:
      return state;
  }
};

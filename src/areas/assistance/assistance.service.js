import { db } from '../../common/firebase';

const pushAssistanceRequest = async position => {
  return await db.ref('/').push({ position });
};

const removeAssistanceRequests = async position => {
  return await db.ref('/').remove();
};

export default { pushAssistanceRequest, removeAssistanceRequests };

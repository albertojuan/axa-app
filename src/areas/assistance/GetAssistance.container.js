import { connect } from 'react-redux';
import GetAssistanceComponent from './GetAssistance.component';
import { showMessageInApp } from '../../common/notifications';
import assistanceActions from './assistance.actions';

const mapStateToProps = (state, props) => ({
  error: state.assistance.error,
  lastUpdate: state.assistance.lastUpdate,
  lastPosition: state.assistance.lastPosition
});

const mapDispatchToProps = (dispatch, ownProps) => ({
  onSubmit: () => {
    navigator.geolocation.getCurrentPosition(position => {
      showMessageInApp(
        dispatch,
        `Requested (${position.coords.latitude},${position.coords.longitude})`
      );
      dispatch(
        assistanceActions.requestAssistance({
          latitude: position.coords.latitude,
          longitude: position.coords.longitude
        })
      );
    });
  },
  onDelete: () => {
    showMessageInApp(dispatch, 'Deleting all requests');
    dispatch(assistanceActions.deleteAssistanceRequests());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GetAssistanceComponent);

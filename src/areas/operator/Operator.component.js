import React from 'react';
import PropTypes from 'prop-types';
import { ReactBingmaps } from 'react-bingmaps';
import styled from 'styled-components';
import withPositionSubscription from '../../common/firebase/withPositionSubscription.hoc';

const MainPanel = styled.div`
  height: 70vh;
  background-color: white;
  overflow: auto;
`;

//TODO: Extract default center and bingmapKey to a config file
function getCenter(positions) {
  const center = positions.length
    ? positions[positions.length - 1]
    : { latitude: 40.4168, longitude: -3.7038 };

  return [center.latitude, center.longitude];
}

const OperatorPanel = ({ positions }) => (
  <MainPanel className="showable">
    <ReactBingmaps
      bingmapKey="ArRRj8-mD3eJgM57MOzaHumBHl6WMvfpf_P6yD1c7Ylpz8OxJVFXo9xVUuncKkhc"
      center={getCenter(positions)}
      zoom={8}
      pushPins={positions.map(x => ({
        location: [x.latitude, x.longitude]
      }))}
    />
  </MainPanel>
);

OperatorPanel.propTypes = {
  positions: PropTypes.array.isRequired
};

export default withPositionSubscription(OperatorPanel);

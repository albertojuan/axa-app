import Operator from './Operator.component';
import React from 'react';
import { shallow } from 'enzyme';

describe('Operator component', () => {
  it('should render Operator component correctly with no props', () => {
    const component = shallow(<Operator positions={[]} />);
    expect(component).toMatchSnapshot();
  });
});

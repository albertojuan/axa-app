import React from 'react';
import styled from 'styled-components';

const MainPanel = styled.div`
  height: 70vh;
  background-color: #fff;
  overflow: auto;
  padding: 10px;
  & > h2 {
    color: #00008f;
  }
`;

const AboutAsComponent = () => {
  return (
    <MainPanel>
      <h2>About this app</h2>
      <p>
        This app was made to show the use of some technologies. It's not a
        serious app.
      </p>

      <p>
        It has two main parts, that in a real scenario will would need
        authentication and may be will be 2 different apps:
      </p>
      <ul>
        <li>
          <p>
            <strong>Client part:</strong> Where clients can request assistance
            sending their current position from their smartphone or other
            devices.
            <br /> Clients can remove their requests too
          </p>
        </li>
        <li>
          <p>
            <strong>Operator panel:</strong> This could be another app, but for
            demo purposes is in the same one. Here operators can see the
            geolocalizated requests.
            <br />
            All requests are shown in a map, centered in the last one.
          </p>
        </li>
      </ul>
      <p>
        To test this app, open the "client" menu in one device and "operator" in
        a second device, then click request "I need assistance" in the first
        device and the second will show immediately the position of the request.
      </p>
      <p>
        More information and source code:
        <a href="https://gitlab.com/albertojuan/axa-app">
          https://gitlab.com/albertojuan/axa-app
        </a>
      </p>
      <p>&nbsp;</p>
    </MainPanel>
  );
};

export default AboutAsComponent;

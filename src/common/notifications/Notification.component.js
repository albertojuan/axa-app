import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import PropTypes from 'prop-types';

class NotificationComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  handleClose = () => {
    this.props.onClose();
  };

  render() {
    const message = this.props.message;
    return (
      <Snackbar
        anchorOrigin={{ vertical: 'bottom', horizontal: 'center' }}
        open={!!message}
        onClose={this.handleClose}
        ContentProps={{
          'aria-describedby': 'message-id'
        }}
        autoHideDuration={2000}
        message={message}
      />
    );
  }
}

NotificationComponent.propTypes = {
  message: PropTypes.string
};

export default NotificationComponent;

import { connect } from 'react-redux';
import notificationActions from './notification.actions';
import NotificationComponent from './Notification.component';

const mapStateToProps = state => {
  return {
    message: state.notification.message
  };
};

const mapDispatchToProps = dispatch => ({
  onClose() {
    dispatch(notificationActions.closeNotification());
  }
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationComponent);

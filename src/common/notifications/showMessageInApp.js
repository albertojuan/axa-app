import util from 'util';
import notificationActions from './notification.actions';

function showMessageInApp(dispatch, message, ...params) {
  dispatch(notificationActions.notify(util.format(message, ...params)));
}

export default showMessageInApp;

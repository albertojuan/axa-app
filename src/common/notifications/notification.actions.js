export const actionTypes = {
  NOTIFY: 'NOTIFY',
  CLOSE_NOTIFICATION: 'CLOSE_NOTIFICATION'
};

export default {
  notify: message => ({
    type: actionTypes.NOTIFY,
    message
  }),
  closeNotification: () => ({
    type: actionTypes.CLOSE_NOTIFICATION
  })
};

import { actionTypes } from './notification.actions';
export default (state = {}, action) => {
  switch (action.type) {
    case actionTypes.NOTIFY:
      return { ...state, message: action.message };

    case actionTypes.CLOSE_NOTIFICATION:
      return { ...state, message: null };

    default:
      return state;
  }
};

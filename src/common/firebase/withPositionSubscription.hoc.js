import React from 'react';
import { db } from './firebase';
import { showMessageInApp } from '../../common/notifications';
import store from '../../state/store';

function withPositionSubscription(WrappedComponent) {
  const ref = db.ref('/');

  // FIXME: Workaround for this demo, because of a bug in bingmaps component
  let firstSubs = true;

  return class extends React.Component {
    constructor(props) {
      super(props);
      this.addPosition = this.handleChange.bind(this);
      this.state = {
        positions: [],
        first: true
      };
    }

    handleChange(positions) {
      if (!this.state.first) {
        showMessageInApp(
          store.dispatch,
          positions ? 'New request received' : 'Requests removed'
        );
      }
      this.setState({
        positions: positions
          ? Object.values(positions).map(x => x.position)
          : [],
        first: false
      });
    }

    componentDidMount() {
      // FIXME: Workaround for this demo, because of a bug in bingmaps component
      const subs = () =>
        ref.on('value', snapshot => {
          this.handleChange(snapshot.val());
        });

      // FIXME: Workaround for this demo, because of a bug in bingmaps component
      if (firstSubs) {
        setTimeout(() => {
          subs();
          firstSubs = false;
        }, 3000);
      } else {
        subs();
      }
    }

    componentWillUnmount() {
      ref.off();
    }

    render() {
      return (
        <WrappedComponent positions={this.state.positions} {...this.props} />
      );
    }
  };
}

export default withPositionSubscription;

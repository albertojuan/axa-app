import styled from 'styled-components';

export default styled.footer`
  position: fixed;
  background-color: #00008f;
  color: white;
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  bottom: 0;
  height: 50px;
  animation-name: hideshow;
  animation-duration: 1s;
  animation-delay: 4s;
  animation-fill-mode: both;
`;

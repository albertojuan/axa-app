import styled, { css } from 'styled-components';

export default styled.p`
  text-align: center;
  ${props =>
    props.important &&
    css`
      font-weight: bolder;
    `};
  ${props =>
    props.error &&
    css`
      color: red;
    `}
`;

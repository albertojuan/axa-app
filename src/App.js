import React, { Component, Fragment } from 'react';
import { Provider } from 'react-redux';
import { Notification } from './common/notifications';
import { BrowserRouter, Route, Link } from 'react-router-dom';
import { AboutAs, GetAssistance, Operator } from './areas';
import myStore from './state/store';
import Logo from './common/logo';
import { Header, Footer, MenuItem } from './common/ui';

class App extends Component {
  render() {
    return (
      <Provider store={myStore}>
        <BrowserRouter>
          <Fragment>
            <Header>
              <Logo />
            </Header>
            <div id="main">
              <Route exact path={'/'} component={GetAssistance} />
              <Route path={'/about-as'} component={AboutAs} />
              <Route path={'/operator'} component={Operator} />
            </div>
            <Notification />
            <Footer>
              <MenuItem>
                <Link to="/">Client</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/operator">Operator panel</Link>
              </MenuItem>
              <MenuItem>
                <Link to="/about-as">About this app</Link>
              </MenuItem>
            </Footer>
          </Fragment>
        </BrowserRouter>
      </Provider>
    );
  }
}

export default App;

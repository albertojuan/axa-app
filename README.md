## About this app

This app was made to show the use of some technologies. It's not a serious app.

It has divided in two main parts which in a real scenario would need authentication and would be 2 different apps:

- **Client part:** Where clients can request assistance sending their current position from their smartphone or other devices.  

- **Operator panel:** This could be another app but for demo purposes is in the same one. Here operators can see the geolocalized requests.  
  All requests are shown in a map which is centered in the last one.

To test this app, open the "client" menu in one device and "operator" in a second device, then click "I need assistance" in the first device and the second will show immediately the position of the request in a map.

[App for client](https://assistanceapp-axa.firebaseapp.com)

[App for operator](https://assistanceapp-axa.firebaseapp.com/operator)

## Technology behind this app

### React

Class components for stateful components (container components) and functional components for presentation ones.

Example of Class component:

```javascript
class NotificationComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = { open: false };
  }

  handleClose = () => {
    this.props.onClose();
  };
  ...
}
```

Example of functional component:

```javascript
const OperatorPanel = ({ positions }) => (
  <MainPanel className="showable">
    <ReactBingmaps
      bingmapKey="..."
      center={getCenter(positions)}
      zoom={8}
      pushPins={positions.map(x => ({
        location: [x.latitude, x.longitude]
      }))}
    />
  </MainPanel>
);
```

### React High Order Component

A React HoC was developed to suscribe the Operator component to the requests positions database in Firebase. The goal is to have real time updates in the component.

### Firebase Real Time Database

A good Database for RTC communication between apps. The official Javascript client has an offline mode that stores all the operations and bulk them when connection is recovered.

### Styles (Styled-components & CSS modules)

For React components the election was Styled-components. The paradigm of CSS in the Javascript can be as powerful as JSX (HTML in Javascript). The reason is you can based the styled based in given props and of course avoid unwanted style changes in other components changing styles from one of them.

Example:

```javascript
export default styled.p`
  text-align: center;
  ${props =>
    props.important &&
    css`
      font-weight: bolder;
    `};
  ${props =>
    props.error &&
    css`
      color: red;
    `}
`;
```

### Redux

The most used library for app state management, Redux has too much boilerplate for this small application but the purpose is to demonstrate how to use it.

### redux-thunk

I personally prefer redux-observable, for me is cleaner because you keep the action creators pure and more powerful because you can handle actions as an input stream.

But in this example I wanted to show redux-thunk that can be a perfect solution when it fits, because of its simplicity.

### react-bingmaps

A fast way to easily use bingmaps with React... but maybe not the best one because it's not very maintained and I found some tricky bugs.

### Enzyme

Very good way to test components:

```javascript
it('should call onSubmit callback when assistance is required (button clicked)', () => {
  const onSubmitFn = jest.fn();

  const component = mount(
    <GetAssistance onSubmit={onSubmitFn} onDelete={jest.fn()} />
  );
  component.find('button#requestBtn').simulate('click');
  expect(onSubmitFn).toHaveBeenCalled();
});
```

### SVG & CSS animations

Some CSS animations were applied to the SVG Axa logo.

### File structure

I chose to have folders by functionality areas rather than by type of files (containers, components, reducers ...). I find it's easier to work by functional contexts and to split the app in smaller apps if it was needed.

I like (and it's standard in React) to use lowerCamelCase for files except in ones returning classes or components (upperCamelCase).

I'm using double extension for some files to show if the file is an action, actionType, reducer, service, component, container, test ...
